
function createCard(name, description, pictureUrl, starts, ends, locationName) {
    return `
      <div class="card-deck shadow-lg">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer bg-transparent border-success">${new Date(starts).toDateString()}-${new Date(ends).toDateString()}</div>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async() => {

    const url = 'http://localhost:8000/api/conferences/';

    try {

      const response = await fetch(url);

      if (!response.ok) {



     } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const locationName = details.conference.location.name;
            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const html = createCard(name, description, pictureUrl, starts, ends, locationName);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {

    console.error(e);


    }

});
